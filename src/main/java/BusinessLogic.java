import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BusinessLogic {
    private WebDriver driver;

    private boolean isLogin = false;

    private WebDriver startup() {

        WebDriver driver;
        System.setProperty("webdriver.chrome.driver", "\\JavaProjects\\Selenium_2.0\\WebDrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.google.com/intl/ru/gmail/about/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        return driver;
    }

    BusinessLogic() {
        this.driver = startup();
    }

    public void login(String userLogin, String userPassword) {
        GmailMain gmailMain = new GmailMain(driver);
        LoginPage loginPage = gmailMain.clickLogin();
        PasswordPage passwordPage = loginPage.sendLogin(userLogin);
        passwordPage.sendPassword(userPassword);

        isLogin = true;
    }

    public void sendMessage(String to, String subject, String text) {
        if(isLogin == true) {
            InboxMails mailsPage = new InboxMails(driver);
            MailForm mailForm = mailsPage.sendMailBtn();
            mailForm.sendMessage(to, subject, text);
        }
    }

    public void restHomework(String to, String subject, String text) {
        InboxMails inboxMails = new InboxMails(driver);
        SentMails sentMails = inboxMails.goToSentMails();
        if(sentMails.compare(to, subject, text)) {
            sentMails.deleteLastMail();
        }
        sentMails.deleteLastMail();
    }
}
