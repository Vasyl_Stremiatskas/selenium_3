import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class InboxMails {
    private WebDriver driver;

    private WebElement sendMailBtn;
    private WebElement sentMails;


    InboxMails(WebDriver driver) {
        this.driver = driver;
        setSendMailBtn();
        locateSentMails();
    }

    private void setSendMailBtn() {
        sendMailBtn = driver.findElement(By.xpath("//*[@id=\":h5\"]/div/div"));
    }

    private void locateSentMails() {
        sentMails = driver.findElement(By.xpath("//*[@id=\":hh\"]/div/div[2]"));
    }

    public MailForm sendMailBtn() {
        sendMailBtn.click();

        MailForm mailForm = new MailForm(driver);
        return mailForm;
    }

    public SentMails goToSentMails() {
        sentMails.click();
        SentMails sentMails = new SentMails(driver);
        return  sentMails;
    }

}


