import org.openqa.selenium.WebElement;

public class MailInfo {
    public String to;
    public String subject;
    public String text;
    public WebElement mailLocator;
}
