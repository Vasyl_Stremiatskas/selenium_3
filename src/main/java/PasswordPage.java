import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PasswordPage {
    private WebDriver driver;
    private WebElement passwordField;

    PasswordPage(WebDriver driver) {
        this.driver = driver;
        setPasswordField();
    }

    private void setPasswordField() {
        passwordField = driver.findElement(By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input"));
    }

    public InboxMails sendPassword(String password) {
        passwordField.sendKeys(password);
        passwordField.sendKeys(Keys.ENTER);

        InboxMails maailsPage = new InboxMails(driver);
        return maailsPage;
    }
}
