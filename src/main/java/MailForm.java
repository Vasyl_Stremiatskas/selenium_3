import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MailForm {
    private WebDriver driver;

    private WebElement toField;
    private WebElement subjectField;
    private WebElement textField;
    private WebElement sendBtn;

    MailForm(WebDriver driver) {
        this.driver = driver;
        setToField();
        setSubjectField();
        setTextField();
        setSendBtn();
    }

    private void setToField() {
        toField = driver.findElement(By.xpath("//*[@id=\":nb\"]"));
    }

    private void setSubjectField() {
        subjectField = driver.findElement(By.xpath("//*[@id=\":mt\"]"));
    }

    private void setTextField() {
        textField = driver.findElement(By.xpath("//*[@id=\":ny\"]"));
    }

    private void setSendBtn() {
        sendBtn = driver.findElement(By.xpath("//*[@id=\":mj\"]"));
    }

    public void sendMessage(String to, String subject, String text) {
        toField.sendKeys(to);
        subjectField.sendKeys(subject);
        textField.sendKeys(text);
        sendBtn.click();
    }
}
