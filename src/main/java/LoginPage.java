import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
    private WebDriver driver;
    private WebElement loginField;

    LoginPage(WebDriver driver) {
        this.driver = driver;
        setLoginField();

    }

    private void setLoginField() {
        loginField = driver.findElement(By.xpath("//*[@id=\"identifierId\"]"));
    }

    public PasswordPage sendLogin(String login) {
        loginField.sendKeys(login);
        loginField.sendKeys(Keys.ENTER);

        PasswordPage passwordPage = new PasswordPage(driver);
        return passwordPage;
    }
}
