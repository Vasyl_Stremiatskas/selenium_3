import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;


public class SentMails {
    private WebDriver driver;

    MailInfo lastMail;
    WebElement deleteBtn;

    SentMails(WebDriver driver) {
        this.driver = driver;
        getLastMail();
        locateDeleteBtn();
    }

    private void locateDeleteBtn() {
        deleteBtn = driver.findElement(By.xpath("//*[@id=\":4\"]/div[2]/div[1]/div[1]/div/div/div[2]/div[3]/div/div"));
    }

    private void getLastMail() {
        lastMail.to = driver.findElement(By.xpath("//*[@id=\":np\"]/span")).getText();
        lastMail.subject = driver.findElement(By.xpath("//*[@id=\":nm\"]/span")).getText();
        lastMail.text =  driver.findElement(By.xpath("//*[@id=\":no\"]/div/div/span")).getText();
        lastMail.mailLocator = driver.findElement(By.xpath("//*[@id=\":ns\"]"));
    }



    public boolean compare(String to, String subject, String text) {
        if(to.equals(lastMail.to) && subject.equals(lastMail.subject) && text.equals(lastMail.text)) {
            return true;
        }
        else {
            return false;
        }
    }

    private void checkLastMessage() {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("document.getElementById(':ns').setAttribute('checked','true')");
    }

    public void deleteLastMail() {
        checkLastMessage();
        deleteBtn.click();
    }


}
