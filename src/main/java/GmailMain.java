import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GmailMain {
    private WebDriver driver;
    private WebElement loginBtn;

    GmailMain(WebDriver driver) {
        this.driver = driver;
        locateLoginBtn();
    }

    private void locateLoginBtn() {
        loginBtn = driver.findElement(By.xpath("/html/body/nav/div/a[2]"));
    }

    public LoginPage clickLogin() {
        loginBtn.click();
        LoginPage loginPage = new LoginPage(driver);
        return loginPage;
    }



}
